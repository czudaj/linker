# linker

Simple rss viewer built with Flutter.io.

## Additional informations

Runs on Android and iOS devices. I tested app only on Android devices, cause I don't have mac yet. So Android is preferd.

- Code is licensed with an open source license - Apache License, Version 2.0.

- This app require Internet connection.

- There's bunch of predefined Rss Channels. You can change it in "urls.dat" file. Those channels are loaded once and save - after first app start.

- You can freely add new channels and delete ones.

- Any changes are saved to SharedPreferences.

- Application supports RSS and doesn't support Atom Feed.

- Thumbnail for a news is shown if it is in: "enclosure.url" or "media.thumbnails.first.url" or "description" - html - tag "img", attribute "src" (html must be valid), otherwise default thumbnail will be showned.

- You can drag a list to refresh chanels.

- Total Dart Code Size: 4971 bytes (without indentations).

Application was build for contest purpose: https://flutter.dev/create.

## Screenshot - phone [Android]

![splash](https://bitbucket.org/czudaj/linker/downloads/1.png)
![main_android](https://bitbucket.org/czudaj/linker/downloads/2.png)
![add](https://bitbucket.org/czudaj/linker/downloads/3.png)

## Screenshot - tablet 8" [Android]

![vertical_tab](https://bitbucket.org/czudaj/linker/downloads/5.jpeg)
![horizontal_tab](https://bitbucket.org/czudaj/linker/downloads/4.jpeg)
