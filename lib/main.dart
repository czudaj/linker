import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:webfeed/webfeed.dart';
import 'package:xml/xml.dart' as Xml;

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primarySwatch: Colors.yellow,
      ),
      home: MainWidget(),
    );
  }
}

class MainWidget extends StatefulWidget {
  @override
  MainWidgetState createState() => MainWidgetState();
}

class Content {
  String url;
  RssFeed feed;

  Content(this.url, this.feed);
}

class MainWidgetState extends State<MainWidget> {
  final Color _accentColor = Color(0xFFFFDD00);

  final List<Content> _channels = <Content>[];
  final _http = Client();
  final _controller = TextEditingController();
  List<String> _urls;
  SharedPreferences _prefs;
  bool _add = false;

  @override
  Widget build(_) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: _accentColor,
        bottom: _add
            ? PreferredSize(
                child: Padding(
                  padding: EdgeInsets.fromLTRB(16.0, 0, 16.0, 16.0),
                  child: TextField(
                    controller: _controller,
                    autofocus: true,
                  ),
                ),
                preferredSize: Size.fromHeight(48.0))
            : null,
        title: Text(_add ? "Add URL" : "Linker"),
        actions: <Widget>[
          Visibility(
            child:
                IconButton(icon: Icon(Icons.check), onPressed: () => _addUrl()),
            visible: _add,
          ),
          IconButton(
              icon: Icon(_add ? Icons.clear : Icons.add),
              onPressed: () => setState(() => _add = !_add)),
        ],
      ),
      body: _buildVerticalList(),
    );
  }

  Widget _buildVerticalList() {
    return Container(
      child: RefreshIndicator(
        child: ListView.builder(
          itemCount: _channels.length,
          itemBuilder: (_, index) => _buildHorizontalList(_channels[index]),
        ),
        onRefresh: _getContent,
      ),
    );
  }

  Widget _buildHorizontalList(Content content) {
    return Column(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.all(16.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                content.feed.title,
                style: TextStyle(fontSize: 15.0),
              ),
              InkWell(
                child: Text(
                  "DELETE",
                  style: TextStyle(
                      fontSize: 12.0,
                      color: _accentColor,
                      fontWeight: FontWeight.bold),
                ),
                onTap: () => _removeUrl(content),
              ),
            ],
          ),
        ),
        Padding(
          padding: EdgeInsets.only(left: 16.0),
          child: SizedBox(
            height: 136.0,
            child: ListView.separated(
              scrollDirection: Axis.horizontal,
              itemCount: content.feed.items.length,
              separatorBuilder: (c, i) => Container(padding: EdgeInsets.all(4)),
              itemBuilder: (_, index) =>
                  _buildListItem(content.feed.items[index]),
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildListItem(RssItem item) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(8.0),
      child: GestureDetector(
        child: Container(
          alignment: Alignment.bottomLeft,
          padding: EdgeInsets.all(8.0),
          width: 164.0,
          decoration: BoxDecoration(
            image: DecorationImage(
              fit: BoxFit.cover,
              colorFilter: ColorFilter.mode(
                  Colors.black.withOpacity(0.25), BlendMode.darken),
              image: _getImg(item) == null
                  ? AssetImage("assets/images/no_image.jpg")
                  : CachedNetworkImageProvider(_getImg(item)),
            ),
          ),
          child: Text(
            item.title,
            maxLines: 2,
            overflow: TextOverflow.ellipsis,
            style: TextStyle(
                fontSize: 14.0,
                color: Colors.white,
                fontWeight: FontWeight.bold),
          ),
        ),
        onTap: () => _launchURL(item.link),
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    _prepareData();
  }

  void _prepareData() async {
    _prefs = await SharedPreferences.getInstance();
    await _loadUrls();
    await _getContent();
  }

  Future<void> _loadUrls() async {
    if (!_prefs.getKeys().contains("urls")) {
      var data = await rootBundle.loadString("assets/urls.dat");
      _urls = data.split("|");
      _saveUrls();
    } else {
      _urls = _prefs.getStringList("urls");
    }
  }

  void _saveUrls() => _prefs.setStringList("urls", _urls);

  _addUrl() {
    _add = false;
    _urls.add(_controller.text);
    _saveUrls();
    _controller.text = "";
    _getContent();
  }

  _removeUrl(Content feed) {
    _urls.remove(feed.url);
    _saveUrls();
    setState(() => _channels.remove(feed));
  }

  Future<void> _getContent() async {
    _channels.clear();
    _urls.forEach((url) async {
      var rss;
      try {
        var response = await _http.get(url);
        rss = RssFeed.parse(response.body);
      } catch (e) {
        rss = RssFeed(title: "Error: " + url, items: <RssItem>[]);
      }
      setState(() => _channels.add(Content(url, rss)));
    });
  }

  String _getImg(RssItem item) {
    try {
      return item.enclosure.url;
    } catch (_) {
      try {
        return item.media.thumbnails.first.url;
      } catch (_) {
        try {
          return Xml.parse("<r>" + item.description + "</r>")
              .findAllElements("img")
              .first
              .getAttribute("src");
        } catch (_) {
          return null;
        }
      }
    }
  }

  Future<void> _launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    }
  }
}
